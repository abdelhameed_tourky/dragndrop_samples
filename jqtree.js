var data = [
    {
        label: 'node1',
        children: [
            { label: 'child1' },
            { label: 'child2' }
        ]
    },
    {
        label: 'node2',
        children: [
            { label: 'child3' }
        ]
    }
];

var data2 = [
    {
        label: 'node11',
        children: [
            { label: 'child11' },
            { label: 'child22' }
        ]
    },
    {
        label: 'node22',
        children: [
            { label: 'child33' }
        ]
    }
];

$(function() {
    $('#tree1').tree({
        dragAndDrop: true,
        data: data
    });

    $('#tree2').tree({
        dragAndDrop: true,
        data: data2,
        onCreateLi: function(node, $li) {
        // Add 'icon' span before title
        // console.log(node);
        // console.log($li[0]);
        // if(node.children.length > 0)
        //     $li.find('.jqtree-title').before('<h5>lol</h5>').after('<h5>....</h5>');
        // $li.find(".jqtree-element").prop("id",node.id);
        var item = $li.find(".jqtree-element")[0];
        console.log(item);
        // item.droppable();
        item.sortable({});
        // item.droppable({
        //   hoverClass: "ui-state-active",
        //   drop: function (event, ui) {
        //     var id = $(this).prop("id");
        //       alert(id);
        //       }
        //     });
    }
    });
});