$(document).ready(function() {

	$( ".item" ).draggable({
        connectToSortable: "#menu ul",
        revert: "invalid"
    });

	$( "#menu ul" ).sortable({
        revert: true,
        connectWith: "#menu ul",
        placeholder: "ui-state-highlight"
    });

    $( ".draggable" ).draggable({
        connectToSortable: "#menu ul",
        helper: "clone",
        revert: "invalid"
    });

    $( "ul, li" ).disableSelection();

	// var temp, source, fromItems = false;
	// $( ".sortable1, .sortable2" ).sortable({
 //      connectWith: ".connectedSortable",
 //      helper: "clone",
 //      start: dragged,
 //      receive: dropped,
 //      remove: removing
 //    }).disableSelection();

	// function dropped(e, ui)
	// {
	// 	console.log("dropped");
	// 	console.log(ui.item);
	// 	ui.item.show();
	// 	source = temp;
	// 	// console.log(e);
	// 	// console.log(ui);
	// 	// console.log(this);
	// }

	// function dragged(e, ui)
	// {
	// 	console.log("dragged");
	// 	console.log(ui.item);
	// 	ui.item.show();
	// 	source = this;
	// 	temp = this.clone(true);
	// 	// console.log(e);
	// 	// console.log(ui);
	// 	// console.log(this);
	// 	// if( this.classList.contains("sortable1") ){}
	// }

	// function removing(e, ui)
	// {
	// 	console.log("removing");
	// 	console.log(ui.item);
	// 	$(this).sortable('cancel');
	// 	// ui.item.show();
	// 	// console.log(e);
	// 	// console.log(ui);
	// 	// console.log(this);
	// 	// if( this.classList.contains("sortable1") ){}
	// }


});