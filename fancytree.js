var data = 
[
	{"title": "///", "expanded": true, "folder": true, "children": [
		{"title": "dev", "folder": false},
		{"title": "etc", "folder": true, "children": [
			{"title": "cups"},
			{"title": "httpd"},
			{"title": "init.d"}
		]},
		{"title": "sbin", "folder": true},
		{"title": "tmp", "folder": true},
		{"title": "Users", "expanded": true, "folder": false, "children": [
			{"title": "jdoe", "folder": false},
			{"title": "jmiller", "folder": true},
			{"title": "mysql", "folder": false}
		]},
		{"title": "usr", "expanded": true, "folder": true, "children": [
			{"title": "bin", "folder": true},
			{"title": "lib", "folder": true},
			{"title": "local", "folder": true}
		]},
		{"title": "var", "expanded": true, "folder": true, "children": [
			{"title": "log", "folder": true},
			{"title": "spool", "folder": true},
			{"title": "yp", "folder": true}
		]}
	]}
];
var menuTree;

$(function(){



	menuTree = $("#treetable").fancytree({
      extensions: ["table","dnd", "edit"],
      checkbox: true,
      table: {
        indentation: 20,      // indent 20px per node level
        nodeColumnIdx: 2,     // render the node title into the 2nd column
        checkboxColumnIdx: 0  // render the checkboxes into the 1st column
      },
      dnd: {
        autoExpandMS: 400,
        focusOnClick: true,
        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
        preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
        dragStart: function(node, data) {
          /** This function MUST be defined to enable dragging for the tree.
           *  Return false to cancel dragging of node.
           */
          return true;
        },
        dragEnter: function(node, data) {
          /** data.otherNode may be null for non-fancytree droppables.
           *  Return false to disallow dropping on node. In this case
           *  dragOver and dragLeave are not called.
           *  Return 'over', 'before, or 'after' to force a hitMode.
           *  Return ['before', 'after'] to restrict available hitModes.
           *  Any other return value will calc the hitMode from the cursor position.
           */
          // Prevent dropping a parent below another parent (only sort
          // nodes under the same parent)
/*           if(node.parent !== data.otherNode.parent){
            return false;
          }
          // Don't allow dropping *over* a node (would create a child)
          return ["before", "after"];
*/
           return true;
        },
        dragDrop: function(node, data) {
          /** This function MUST be defined to enable dropping of items on
           *  the tree.
           */
           console.log("menu drag dropped");
          if(data.otherNode.tree.$div[0].id == "reusable")
          {
          	data.otherNode.copyTo(node, data.hitMode, function(n){
		      n.title = "Copy of " + n.title;
		      n.key = null; // make sure, a new key is generated
		    });
          }
          else if(data.otherNode.tree.$div[0].id == "vam_objects")
          {
          	data.otherNode.moveTo(node, data.hitMode);
          }
          else
          {
          	data.otherNode.moveTo(node, data.hitMode);
          }
          // data.otherNode.copyTo(node, data.hitMode);
        }
      },
      source: data,
      lazyLoad: function(event, data) {
        data.result = {url: "ajax-sub2.json"}
      },
      renderColumns: function(event, data) {
        var node = data.node,
          $tdList = $(node.tr).find(">td");
        // (index #0 is rendered by fancytree by adding the checkbox)
        $tdList.eq(1).text(node.getIndexHier()).addClass("alignRight");
        // (index #2 is rendered by fancytree)
        $tdList.eq(3).text(node.key+"..");
        $tdList.eq(4).html("<input type='checkbox' name='like' value='" + node.key + "'>");
        $tdList.eq(5).text("hello!!");
        if(node.key != "_1")
	        $tdList.eq(6).html("<input name='delete' type='button' value='delete' />");
      }
    });
	


    /* Handle custom checkbox clicks */
    $("#treetable").delegate("input[name=delete]", "click", function(e){
      var node = $.ui.fancytree.getNode(e),
         $input = $(e.target);
      e.stopPropagation();  // prevent fancytree activate for this row
      node.remove();
    });




    // Attach the fancytree widget to an existing <div id="tree"> element
    // and pass the tree options as an argument to the fancytree() function:
    $("#reusable, #vam_objects").fancytree({
      extensions: ["dnd"],
      // titlesTabbable: true,
      source: data,
      dnd: {
        autoExpandMS: 400,
        focusOnClick: true,
        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
        preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
        dragStart: function(node, data) {
          /** This function MUST be defined to enable dragging for the tree.
           *  Return false to cancel dragging of node.
           */
          return true;
        },
        dragEnter: function(node, data) {
          /** data.otherNode may be null for non-fancytree droppables.
           *  Return false to disallow dropping on node. In this case
           *  dragOver and dragLeave are not called.
           *  Return 'over', 'before, or 'after' to force a hitMode.
           *  Return ['before', 'after'] to restrict available hitModes.
           *  Any other return value will calc the hitMode from the cursor position.
           */
          // Prevent dropping a parent below another parent (only sort
          // nodes under the same parent)
/*           if(node.parent !== data.otherNode.parent){
            return false;
          }
          // Don't allow dropping *over* a node (would create a child)
          return ["before", "after"];
*/
           return true;
        },
        dragDrop: function(node, data) {
          /** This function MUST be defined to enable dropping of items on
           *  the tree.
           */
          // data.otherNode.moveTo(node, data.hitMode);
          console.log(node.FancyTree);
          console.log(data.otherNode.FancyTree);
          data.otherNode.copyTo(node, data.hitMode);
        }
      },
      activate: function(event, data) {
//        alert("activate " + data.node);
      },
      lazyLoad: function(event, data) {
        data.result = {url: "ajax-sub2.json"}
      }
    });
  });